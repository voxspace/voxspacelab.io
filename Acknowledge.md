# Acknowledge

VoxSpace is inspired by [fluid-engine-dev](https://github.com/doyubkim/fluid-engine-dev) which teach me how to code
numerical algorithm by using C++. In this repo, files in src.common and src.lagrange folder are all forked
from [fluid-engine-dev(branches dev-v2-gpu)](https://github.com/doyubkim/fluid-engine-dev).

Thanks to [Guanghui Hu](https://www.fst.um.edu.mo/people/garyhu/) and [Ruo Li](http://dsec.pku.edu.cn/~rli/index.php)
who teach me how to make researches in the numerical simulations and [AFEPack](https://github.com/wangheyu/AFEPack)
inspires me to start this opensource project.